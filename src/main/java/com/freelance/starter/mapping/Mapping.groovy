package com.freelance.starter.mapping

import com.freelance.starter.product.Product
import groovy.xml.StreamingMarkupBuilder

public interface Mapping {

    void map(Product source, Object target);


}