package com.freelance.starter.product;

import java.util.List;

/**
 * @author ddelizia
 * @since 11/03/15 15:48
 */
public class Product {

    private String productCode;
    private String title;
    private String description;
    private List<Category> categories;


    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(final List<Category> categories) {
        this.categories = categories;
    }
}
