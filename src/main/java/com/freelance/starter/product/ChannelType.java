package com.freelance.starter.product;

/**
 * @author ddelizia
 * @since 11/03/15 15:49
 */
public enum ChannelType{
    GOOGLE("google"),
    AMAZON("amazon"),
    EBAY("ebay");

    private final String code;

    ChannelType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
