package com.freelance.starter.product;

/**
 * @author ddelizia
 * @since 11/03/15 15:48
 */
public class Category {

    private String categoryCode;
    private String categoryDescription;
    private ChannelType channelType;


    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(final String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public void setCategoryDescription(final String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    public ChannelType getChannelType() {
        return channelType;
    }

    public void setChannelType(final ChannelType channelType) {
        this.channelType = channelType;
    }
}
